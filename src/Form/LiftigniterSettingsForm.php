<?php

namespace Drupal\liftigniter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configure Liftigniter settings for this site.
 */
class LiftigniterSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'liftigniter_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'liftigniter.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('liftigniter.settings');

    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Uncheck this box to completely disable LiftIgniter functionality.'),
      '#default_value' => $config->get('enable'),
    ];
    $settings_url = Url::fromUri('https://console.liftigniter.com/settings/keys', [
      'attributes' => [
        'target' => '_blank',
      ],
    ]);
    $settings_link = Link::fromTextAndUrl($this->t('Settings'), $settings_url)->toString();
    $form['js_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('JS Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('js_key'),
      '#description' => $this->t('Enter the JS Key (SDK) of LiftIgniter.</br>JS Key will be found in the @link page of the LiftIgniter console.', [
        '@link' => $settings_link,
      ]),
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('Enter the API Key (REST) of LiftIgniter.</br>API Key will be found in the @link page of the LiftIgniter console.', [
        '@link' => $settings_link,
      ]),
    ];
    $form['request_fields'] = [
      '#type' => 'textfield',
      '#title' => $this->t('String Value Field List'),
      '#description' => $this->t('Additional fields (String value) to be returned from LiftIgniter.</br>Enter each field name seperated by a comma.</br>If this field is set to "<b>name, title, url</b>" the js code will add <b>$p("setRequestFields",["name","title","url"])</b>'),
      '#default_value' => $config->get('request_fields'),
      '#maxlength' => 256,
    ];
    $form['array_request_fields'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Array Value Field List'),
      '#description' => $this->t('Additional fields (Array value) to be returned from LiftIgniter.</br>Enter each field name seperated by a comma.</br>If this field is set to "<b>name, title, url</b>" the js code will add <b>$p("setArrayRequestFields",["name","title","url"])</b>'),
      '#default_value' => $config->get('array_request_fields'),
      '#maxlength' => 256,
    ];
    $form['set_no_tag'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Set No Tag'),
      '#description' => $this->t("This can be used to configure whether LiftIgniter's tags are added to URLs in items recommended by LiftIgniter. By default, (non-UTM) LiftIgniter tags are added to recommendations. You can disable adding tags to recommendations by enabling this field."),
      '#default_value' => $config->get('set_no_tag'),
    ];
    $form['global_config'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom Configuration'),
      '#default_value' => $config->get('global_config'),
      '#description' => $this->t('Enter custom configurations to be added in LiftIgniter script.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve and save the configuration.
    $this->configFactory->getEditable('liftigniter.settings')
      ->set('enable', $form_state->getValue('enable'))
      ->set('js_key', $form_state->getValue('js_key'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('request_fields', $form_state->getValue('request_fields'))
      ->set('array_request_fields', $form_state->getValue('array_request_fields'))
      ->set('set_no_tag', $form_state->getValue('set_no_tag'))
      ->set('global_config', $form_state->getValue('global_config'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
