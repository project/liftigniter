/**
 * @file
 * Contains the definition of the behavior liftigniterWidget.
 */

(function ($, drupalSettings, Drupal) {

  'use strict';

  // Initializing templates and widgets variables.
  var templates = drupalSettings.liftigniter.templates;
  var widgets = drupalSettings.liftigniter.widgets;

  /**
   * Attaches the JS behavior for registering widgets.
   */
  Drupal.behaviors.liftigniterWidget = {
    attach: function (context, settings) {
    }
  };

  /**
   * Adds Anchor / Non-Anchor tracking for the widgets.
   * @param {Object} tracking The tracking object of the widget.
   * @param {string} widgetName The widget Name.
   * @param {string} algorithm The tracking algorithm of the widget.
   * @return {void}
   */
  function trackWidget(tracking, widgetName, algorithm) {
    switch (tracking.method) {
      case 'anchor':
        var track = {
          elements: document.querySelectorAll(tracking.anchor_selector),
          name: widgetName,
          source: algorithm,
          _debug: tracking.debug
        };
        if (tracking.tag_prefix) {
          track.tagPrefix = tracking.tag_prefix;
        }
        $p('track', track);
        break;
      case 'non_anchor':
        // Add Proper Non Anchor Tracking Function.
        // nonAnchorTrackWidget(tracking.selector, widgetName, algorithm, '');// Add options for defualt URL or Inventory URL
        break;
    }
  }

  /**
   * Registering widgets.
   * @param {Object} widget The complete widget object.
   * @return {void}
   */
  function registerWidget(widget) {
    $p('register', {
      max: parseInt(widget.max, 10),
      widget: widget.widget,
      callback: function (resp) {
        if (resp.items && resp.items.length) {
          // Hook to modify response.
          switch (widget.renderer.method) {
            case 'all':
              var $element = $(widget.selector);
              $element.html($p('render', templates[widget.renderer.template].twig, resp));
              break;
            case 'each':
              $(widget.selector).each(function (item_key, element) {
                if (resp.items.length <= item_key) {
                  // Stop rendering as all iteration of the items are rendered.
                  return false;
                }
                $(this).html($p('render', templates[widget.renderer.template].twig, resp.items[item_key]));
              });
              break;
          }
          if (widget.track.li.enable) {
            trackWidget(widget.track.li, widget.widget, 'LI');
          }
        }
      },
      opts: JSON.parse(widget.opts)
    });
  }

  /**
   * Callback function for AB testing.
   * @param {number} slice The current slice value of liftIgniter.
   * @return {void}
   */
  function abTestHandler(slice) {
    $.each(widgets, function (key, widget) {
      if ($(widget.selector).length) {
        if (widget.no_of_items === 'dynamic') {
          widget.max = $(widget.selector).length;
        }
        var registerWidgetFlag = true; // Default to true. register hook to modify this value.
        if (registerWidgetFlag) {
          if (widget.ab_testing.enable) {
            if (slice < widget.ab_testing.slice) {
              registerWidget(widget);
            }
            else {
              if (widget.track.base.enable) {
                trackWidget(widget.track.base, widget.widget, 'base');
              }
            }
          }
          else {
            registerWidget(widget);
          }
        }
      }
    });
    if (drupalSettings.liftigniter.request_fields) {
      $p('setRequestFields', drupalSettings.liftigniter.request_fields);
    }
    if (drupalSettings.liftigniter.array_request_fields) {
      $p('setArrayRequestFields', drupalSettings.liftigniter.array_request_fields);
    }
    $p('fetch');
  }
  $p('abTestSlice', {
    callback: abTestHandler
  });
})(jQuery, drupalSettings, Drupal);
