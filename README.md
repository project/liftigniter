LIFTIGNITER DRUPAL MODULE
=========================

LiftIgniter module integrates LiftIgniter with Drupal using SDK (JavaScript API)
and REST API.

CONTENTS OF THIS FILE
---------------------

 * [Introduction](#introduction)
 * [Core Features & Benefits](#core-features--benefits)
 * [Requirements](#requirements)
 * [Recommended modules](#recommended-modules)
 * [Installation](#installation)
 * [Configuration](#configuration)
 * [SUBMODULE OVERVIEW](#submodule-overview)
 * [Maintainers](#maintainers)

INTRODUCTION
------------

LiftIgniter for Drupal 8 will now integrates LiftIgniter with Drupal using SDK
(JavaScript API).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/liftigniter

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/liftigniter

CORE FEATURES & BENEFITS
------------------------

Some of the common features of LiftIgniter are below.
 - Ability to create Templates and Widgets.
 - Ability to have separate configurations for each widgets.
 - Ability to alter response of each widgets before it is rendered.

REQUIREMENTS
------------

No special requirements.

RECOMMENDED MODULES
-------------------

 * [Metatag](https://www.drupal.org/project/metatag):
   Provides Open Graph Tags which is used for collecting Inventories.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

 * Configure the LiftIgniter Settings in
 Administration » Configuration » Web services » Liftigniter:

   - Settings:  `/admin/config/services/liftigniter/settings`

     Settings Page of LiftIgniter Module. Add the required JS Key, API Key and
     Custom Config to add in JavaScript Snippet.

   - Templates:  `/admin/config/services/liftigniter/template`

     Template listing Page. Add, Edit, Delete Templates used for LiftIgniter
     Widgets.

   - Widgets:  `/admin/config/services/liftigniter/widget`

     Widget listing Page. Add, Edit, Delete Widgets used for LiftIgniter.

 * Configure the LiftIgniter permissions in
 Administration » People » Permissions:

   - Use the administration pages and help (System module)

     The top-level administration categories require this permission to be
     accessible. The administration menu will be empty unless this permission
     is granted.

   - Administer site configuration (System module)

     The configuration page require this permission to be accessible.

   - Administer Liftigniter

     Users with this permission will be able to CRUD Widgets and Templates.

SUBMODULE OVERVIEW
------------------

LiftIgniter comes with quite a few submodules to do various tasks. Details
of the module's are given below. Some modules are under development.

### LiftIgniter UI (liftigniter_ui)

This module provides the user interface for configuring templates and
widgets of LiftIgniter

### LiftIgniter Inventory (liftigniter_inventory)

The content entities of Drupal can be used as inventories using this module.
This is under development and is not usable. It is been hidden in Module
listing page.

MAINTAINERS
-----------

Current maintainers:
 * [Paulraj Augustin](https://www.drupal.org/u/paulraj-augustin)
